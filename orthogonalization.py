from sympy import *
import numpy as np
from numpy import (array, dot, arccos)
from numpy.linalg import norm
init_printing(use_unicode=True)

#m=Matrix([[np.sqrt(3)/2.0,0.5],[np.sqrt(3)/2.0,-0.5]]) Symmetric vectors

m=Matrix([[2.44105,0.0],[1.220525,2.114011312]])

print ("first printing of m")
print (m)

#a, b = m.diagonalize()

c = m[:,:]


non_orth_length1 = m[0] 
non_orth_length2 = m[3]

print (" ")


for i in range(1,5):
	for j in range(1,5):
				m = c[:,:]
				m[0] = m[0]*i
				m[1] = m[1]*i
				m[2] = m[2]*j
				m[3] = m[3]*j
				a, b = m.diagonalize()
				area_orth_supercell = b[0]*b[3]
				#print ("area of orth",b[0]*b[3])
				for ii in range(-10,10):
					for jj in range(-10,10):
						log1 = (abs(b[0] - (c[0]*ii + c[2]*jj)) <= 0.01)
						log4 = (abs(b[3] - (c[1]*ii + c[3]*jj)) <= 0.01)
						if ( log1 and log4 and (area_orth_supercell != 0)):
							#print (ii*c[0,:], jj*c[1,:])
							u = np.array([ii*c[0,:]])
							v = np.array([jj*c[1,:]])
							area_non_orth_supercell = np.cross(u,v)
							print ("bingo" , i, j, ii, jj, b[0]*b[3], area_orth_supercell)
							#print (area_non_orth_supercell,area_orth_supercell)
							#quit()